import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import AppNavBar from './components/AppNavBar';
import Landing from './components/Landing';


const pageComponent = (
	<React.Fragment>
		<BrowserRouter  basename={process.env.PUBLIC_URL}>	
			<AppNavBar/>
			<Switch>
				<Route exact path = "/" component={Landing}/>
			</Switch>
		</BrowserRouter>	
	</React.Fragment>
)

ReactDOM.render(pageComponent, document.getElementById('root'));
