import React from 'react';

const AppNavbar = ()=>{

	return(
		<div className="nav-section" id="navbar-section">
			<nav className="navbar navbar-expand-md navbar-dark fixed-top">

				<a href="./" className="navbar-brand">
					gui'dute					
				</a>

				<button className="navbar-toggler" data-toggle="collapse" data-target="#nav-dropdown">
					<span className="navbar-toggler-icon"></span>
				</button>

				<div id="nav-dropdown" className="collapse navbar-collapse">
					<ul className="navbar-nav ml-auto">
						<li className="nav-item">
							<a href="./" className="nav-link">Home</a>
						</li>
						<li className="nav-item">
							<a href="./#about-container" className="nav-link">About</a>
						</li>
						<li className="nav-item">
							<a href="./#project-container" className="nav-link">Project</a>
						</li>
						<li className="nav-item">
							<a href="./#contact-container" className="nav-link">Contact</a>
						</li>
					</ul>
				</div>

			</nav>
		</div>

	)
}
export default AppNavbar