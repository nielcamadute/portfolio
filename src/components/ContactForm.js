import React, {useState} from 'react';
import ReactDOM from 'react-dom';

const ContactForm = ()=>{

	return(
	    <div className="col-lg-8 col-md-12 col-xs-12 contact-form-col" >
			<form>
				<div className="form-group">
				    <label htmlFor="fullname">Full Name</label>
				    <input type="text" className="form-control" id="fullname" name="fullname" required/>
				    <span id="nameErr" className="alert">
						<small className="text-secondary alert-danger">Please enter your fullname</small>
					</span>	
				</div>
				<div className="form-group">
				    <label htmlFor="email">Email</label>
				    <input type="email" className="form-control" id="email" name="email"  required/>
				    <span id="emailErr" className="alert">
						<small className="text-secondary alert-danger">Please enter valid email</small>
					</span>	
				</div>
				<div className="form-group">
				    <label htmlFor="message">Message</label>
				    <textarea className="form-control" id="message" name="message" required></textarea>
				    <span id="messageErr" className="alert">
						<small className="text-secondary alert-danger">Please leave your message</small>
					</span>	
				</div>
				<button type="submit" className="btn" id="btnContact">Send</button>
			</form>
		</div>
	)
}

export default ContactForm


