import React from 'react';

const ContactInfo = ({contactData})=>{
	return(
		<React.Fragment>
	    	{
	    		contactData.map((contact)=>{
	    			const{email, number, address, linkdin, facebook, instagram} = contact
	    			return(
					    <div className="col-lg-6 col-md-12 col-xs-12 contact-info-col">
					    	<h2>Contact</h2>
			    			<p>	<i className="fas fa-map-marker-alt"></i> {address} </p>
			    			<p>	<i className="fas fa-mobile-alt"></i> {number} </p>
			    			<p>	<i className="fas fa-at"></i> {email} </p>
			    			<p>	<a href={linkdin} target="_blank"><i className="fab fa-linkedin-in"></i>/loreniel-camadute-37749475</a> </p>
			    			<p>	<a href={facebook} target="_blank"><i className="fab fa-facebook"></i>/nakinocam</a> </p>
			    			<p>	<i className="fab fa-instagram"></i> {instagram} </p>
						</div>
	    			)
	    		})
	    	}
		</React.Fragment>
	)
}

export default ContactInfo
