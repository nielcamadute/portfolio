import React from 'react';

const ProjectElement = ()=>{
	return(

	  <div className="row project-element-row">
	    <div className="col-lg-4 col-md-12 col-xs-12">
	    	<div className="project" id="kuyamis-project">
			    <h4>Kuyamis</h4>
		    	<p className="">This site a static website and blog site inspired. It features about Misamis Occidental, tourist spots and delicacies. HTML, CSS, and Bootstrap were used to build this static website.</p>

				<a href="https://nielcamadute.gitlab.io/kuyamis" target="_blank" className="btn">View</a>
	    	</div>
	    </div>

		<div className="col-lg-4 col-md-12 col-xs-12">
	    	<div className="project" id="datustore-project">
			    <h4>Datustore</h4>
		    	<p className="">This site is an assets management. This was created using Laravel and MySQL, and the application is hosted via Heroku and db4free.
		    	</p>
				<a href="https://cryptic-forest-95364.herokuapp.com/" target="_blank" className="btn">View</a>
	    	</div>
	    </div>
	    <div className="col-lg-4 col-md-12 col-xs-12">
	    	<div className="project" id="musixbox-project">
			    <h4>Musixbox</h4>
		    	<p className="">This site is a booking system for a KTV Bar. This was created using MongoDB, Express Js, React JS, Node JS, and GraphQL. It is hosted via Heroku application and Gitlab.
		    	</p>
				<a href="https://nielcamadute.gitlab.io/musixbox/#/" target="_blank" className="btn">View</a>
	    	</div>
	    </div>
	  </div>
	)
}

export default ProjectElement