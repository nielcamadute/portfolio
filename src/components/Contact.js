import React from 'react';
import ContactInfo from './ContactInfo'


const Contact = ()=>{
	let contact = [
		{
			email: 'nielcam88@gmail.com',
			number: '09154071784',
			address: 'Washington St., Pio del Pilar, Makati City, PH',
			linkdin: 'https://www.linkedin.com/in/loreniel-camadute-37749475',
			facebook: 'https://www.facebook.com/nakinocam',
			instagram: '@datunakbanguis'
		}
]
	// <ContactForm/>
	return(
		<div className="container-fluid component-container" id="contact-container">
		  <div className="row">
		  	<div className="col-lg-3 col-md-12 col-xs-12"></div>
			<ContactInfo contactData = {contact}/>
		  	<div className="col-lg-3 col-md-12 col-xs-12"></div>
		  </div>
		</div>
	)
}


export default Contact
