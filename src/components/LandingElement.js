import React from 'react';

const LandingElement = ({landingData})=>{
	return(
		<React.Fragment>
			<div className="container-fluid component-container" id="landing-container">
			  	<div className="row">
			    	{
			    		landingData.map((info)=>{
			    			const {header, name, title} = info
			    			return(
							    <div className="col-lg-12 col-md-12 col-xs-12 landing-box" key="a">
							    	<div className="landing-box-sub">
								        <h1>{header} {name}</h1>
								        <h2>{title}</h2>
		
								        <a className="mouse-scroll hidden-xs dadada" href="./#about-container">
			                                <span className="mouse">
			                                    <span className="mouse-movement"></span>
			                                </span>
			                            </a>
							    	</div>
								</div>
			    			)
			    		})
			    	}
			  	</div>
			</div>
		</React.Fragment>
	)
}
export default LandingElement
