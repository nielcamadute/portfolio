import React from 'react';
import LandingElement from './LandingElement';
import About from './About';
import Contact from './Contact';
import Project from './Project';
import Footer from './Footer';

const Landing = ()=>{
	let landing = [
		{
			header: "Hi, I'm ",
			name: 'Niel',
			title: 'Web Developer',
		}
	]
	
	return(
		<React.Fragment>
			<LandingElement landingData = {landing}/>
			<About/>
			<Project/>
			<Contact/>
			<Footer/>
			

		</React.Fragment>
	)
}
export default Landing

