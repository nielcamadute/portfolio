import React from 'react';

const AboutElement = ({aboutData, skillData})=>{
	const showskills = ()=>{
		document.querySelector('#divskills').style.display = "block"
	}
	const hideskills = ()=>{
		document.querySelector('#divskills').style.display = "none"
	}
	return(
		<div className="row">
			<div className="col-lg-4 col-md-12 col-xs-12 about-element-4">
				<div className="about-element" id="about-img"></div>
			</div>
	    	{
	    		aboutData.map((about)=>{
	    			const{title, description1, description2, fullname, address, number, email} = about

	    			return(
					    <div className="col-lg-8 col-md-12 col-xs-12 about-element-8">
					    	<div className="about-element">
			    				<h2>{title}</h2>
				    			<p>{description1}</p>
				    			<p>{description2}</p>
				    			<div>Name: {fullname}</div>
				    			<div>Email: {email}</div>
				    			<div>Phone: {number}</div>
				    			<div>Address: {address}</div>

				    			<a className="" onClick={()=> showskills()}>Skills</a>
				    			<a className="" href="https://nielcamadute.gitlab.io/resume/" target="_blank" title="Click to view Resume" id="btnresume">Resume</a>
					    	</div>
						</div>
	    			)
	    		})
	    	}

	    	<div className="col-lg-12 col-md-12 col-xs-12" id="divskills">
	    		<div id="divskills-sub">
	    			<h2>SKILLS</h2>
		    		{
			      		skillData.map((skills)=>{
			      			const{lang} = skills
			      			return <label>{lang}</label>
			      		})
			      	}
			      	<div>
				      	<a className="" onClick={()=> hideskills()}>Close</a>
			      	</div>
	    		</div>
			</div>

	    	<div className="modal fade" tabIndex="-1" id="modal-skills">
			  <div className="modal-dialog">
			    <div className="modal-content" id="">
			      <div className="modal-header" id="">
			        <h4 className="modal-title" id="">My Skills</h4>
			        <button type="button" className="close" data-dismiss="modal" aria-label="Close"  id="modal-close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div className="modal-body" id="skillsModalBody">
			      	{
			      		skillData.map((skills)=>{
			      			const{lang} = skills
			      			return <label>{lang}</label>
			      		})
			      	}
			      </div>
			    </div>
			  </div>
			</div>
			
		</div>
	)
}

export default AboutElement