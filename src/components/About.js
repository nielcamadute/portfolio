import React from 'react';
import AboutElement from './AboutElement';

const About = ()=>{
	let about = [
		{
			title: 'Full-Stack Developer',
			description1: 'I am Loreniel Camadute. I am a simple, hard-working, creative, enthusiastic and fun-loving person. I always like to create something on my own which I can be proud of and boost my confidence.',
			description2: 'I had working experience in web development: created a prototype application, performed unit-testing on existing and newly created application, prepared documentation, and developed application. During my free time, I love watching programming tutorials, and enrolled in some bootcamps and online tutorials.',
			fullname: 'Loreniel Camadute',
			email: 'nielcam88@gmail.com',
			number: '09154071784',
			address: 'Washington St, Pio del Pilar, Makati City, PH'

		}
	]
	let skills = [
		{lang: <i className="fab fa-html5 fa-2x"></i>},
		{lang: <i className="fab fa-css3-alt fa-2x"></i>},
		{lang: <i className="fab fa-1x">B</i>},
		{lang: <i className="fab fa-js fa-2x"></i>},
		{lang: <i className="fab fa-angular fa-2x"></i>},
		{lang: <i className="fab fa-react fa-2x"></i>},
		{lang: <i className="fa fa-1x">C#</i>},
		{lang: <i className="fab fa-laravel fa-2x"></i>},
		{lang: <i className="fab fa-php fa-2x"></i>},
		{lang: <i className="fab fa-npm fa-2x"></i>},
		{lang: <i className="fa fa-1x">SQL</i>},
		{lang: <i className="fab fa-1x">NoSQL</i>},
		{lang: <i className="fa fa-1x">MySQL</i>},
	]
                 
	return(
		<React.Fragment>
			<div className="container-fluid component-container" id="about-container">
				<AboutElement aboutData = {about} skillData = {skills}/>
			</div>
		</React.Fragment>
	)
}

export default About




