import React from 'react';
import ProjectElement from './ProjectElement';

const Project = ()=>{
	return(
		<div className="container-fluid component-container" id="project-container">
		  <div className="row">
		    <div className="col-lg-12 col-md-12 col-xs-12">
		    	<h2>My Projects</h2>
		    </div>
		  </div>
		  <ProjectElement/>
		</div>
	)
}

export default Project