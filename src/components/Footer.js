import React from 'react';

const Footer = ()=>{
	return(
		<div className="container-fluid component-footer" id="footer-container">
			<div className="row">
			    <div className="col-lg-4 col-md-4 col-xs-12">
			    	<p>Links</p>
			    	<div><a href="#landing-container">Home</a></div>
			    	<div><a href="#about-container">About</a></div>
			    	<div><a href="https://nielcamadute.gitlab.io/resume/" target="_blank">Resume</a></div>
			    	<div><a href="#project-container">Project</a></div>
			    </div>
			    <div className="col-lg-4 col-md-4 col-xs-12">
			    	<p>Projects</p>
			    	<div><a href="https://nielcamadute.gitlab.io/kuyamis" target="_blank">Kuyamis</a></div>
			    	<div><a href="https://nielcamadute.gitlab.io/covid19screeningtool" target="_blank">C-19 Screening Tool</a></div>
			    	<div><a href="https://cryptic-forest-95364.herokuapp.com/" target="_blank">Datustore</a></div>
			    	<div><a href="https://boiling-falls-42930.herokuapp.com/login" target="_blank">Musixbox</a></div>
			    </div>
			    <div className="col-lg-4 col-md-4 col-xs-12">
			    	<p>Contact</p>
			    	<div><i className="fas fa-map-marker-alt"></i>Washington St., Pio del Pilar, Makati City, PH</div>
			    	<div><i className="fas fa-at"></i>nielcam88@gmail.com</div>
			    	<div><i className="fas fa-mobile-alt"></i>09154071784</div>
			    	<div></div>
			    </div>
			    <div className="col-lg-12 col-md-12 col-xs-12" id="copyright-col">
			    	<p>
			    		<i className="far fa-copyright"></i>2020 gui'dute
			    	</p>
			    </div>
			    
		  	</div>
		</div>
	)
}
export default Footer





